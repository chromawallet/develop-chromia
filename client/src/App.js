import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route, Redirect,
} from "react-router-dom";
import { SSOLogin, SSOSuccess } from './pages/authentication';
import Home from './pages/home';
import { BlockchainProvider } from './lib/blockchain/blockchain-context';
import './App.css';
import { SSO } from 'ft3-lib';

function App() {
  SSO.vaultUrl = process.env.REACT_APP_VAULT_URL;
  return (
    <div className="App">
      <BlockchainProvider>
        <Router>
          <Switch>
            <Route path="/sso/login">
              <SSOLogin />
            </Route>
            <Route path="/sso/success">
              <SSOSuccess />
            </Route>
            <Route path="/" exact>
              <Home/>
            </Route>
            <Redirect to="/" />
          </Switch>
        </Router>
      </BlockchainProvider>
    </div>
  );
}

export default App;
