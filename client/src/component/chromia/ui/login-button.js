import React from 'react';
import {Button} from '@material-ui/core';
import ChromiaLogo from './chromia-logo'
import {makeStyles} from "@material-ui/styles";
import Box from "@material-ui/core/Box";

const lightBg = 'white';
const darkBg = '#1E191E';

const useStyles = makeStyles(() => ({
  text: {
    fontSize: 16,
    fontFamily: 'International,"Roboto","Helvetica",sans-serif',
    textTransform: 'none',
    paddingTop: '5px',
    fontWeight: 600,
    color: (prop) => prop.dark ? lightBg : darkBg,
  },
  button: {
    backgroundClip: 'padding-box',
    overflow: 'hidden',
    borderRadius: 30,
    border: (prop) => `1px ${prop.dark ? lightBg : darkBg} solid`,
    padding: '0 35px 0 0',
    backgroundColor: (prop) => prop.dark ? darkBg : lightBg,
    '&:hover': {
      backgroundColor: (prop) => prop.dark ? darkBg : lightBg,
    }
  }
}));

const LoginButton = ({dark, ...props}) => {
  const classes = useStyles({ dark });
  return (
    <Button
      startIcon={(
        <Box
          width={65}
          height={50}
          display="flex"
          bgcolor={darkBg}
          justifyContent="center"
          alignItems="center"
        >
          <ChromiaLogo style={{marginLeft: dark ? 30 : 12, marginBottom: 2, height: 30, width: 30}}/>
        </Box>
      )}
      className={classes.button}
      {...props}
    >
      <span className={classes.text}>Sign in with Chromia</span>
    </Button>
  )
};


export default LoginButton