# How to run

1. For a clean installation of prostgresql with postchain's default role and database

```
docker run --name postchain -e POSTGRES_INITDB_ARGS="--lc-collate=C.UTF-8 --lc-ctype=C.UTF-8 --encoding=UTF-8" -e POSTGRES_USER=postchain -e POSTGRES_PASSWORD=postchain -p 5432:5432 -d postgres eebb4ecd96c80c438657f72f7572e7af486d74ce6fa7346851569aaa63408199
```
2. Put a copy of the config template (config.template.yml) inside the blockchain project folder. Update the template with configs for your chain and rename it config.yml.

Folder structure:
```
myblockchain/
    - config.yml
    - src/
         -main.rell
         - lib/
              - ft3/
```
3. In the project folder run the following:

```
chr start -s config.yml
```
